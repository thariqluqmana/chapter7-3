import React from "react";
import Chart from "../../component/Chart/Chart";
import Featured from "../../component/Featured/Featured";
import Navbar from "../../component/Navbar/Navbar";
import Sidebar from "../../component/Sidebar/Sidebar";
import Tables from "../../component/table/Tables";
import Widget from "../../component/Widget/Widget";
import "./home.scss";

const Home = () => {
  return (
    <div className="Home">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        <div className="widgets">
          <Widget type="user" />
          <Widget type="order" />
          <Widget type="earning" />
          <Widget type="balance" />
        </div>
        <div className="charts">
          <Featured />
          <Chart />
        </div>
        <div className="listContainer">
          <div className="listTitle">Taulah</div>
          <Tables />
        </div>
      </div>
    </div>
  );
};

export default Home;
