import React, { useContext } from "react";
import Login from "./compenents/Login";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import "./App.css";
import SearchCar from "./compenents/SearchCar";
import ResultSC from "./compenents/ResultSC";
import DetailCarPage from "./compenents/DetailCarPage";
import { composeWithDevTools } from "redux-devtools-extension";
import { createContext, useEffect, useState } from "react";
import ListCarDashboard from "./compenents/ListCarDashboard";
import axios from "axios";
import { userInputs } from "./formSource";
import New from "./pages/neww/New";
import Dashboard from "./compenents/Dashboard";
import { AuthContext } from "./Context/AuthContext";
export const data = createContext();

function App() {
  // const [user, setUser] = useState(null);

  // useEffect(() => {
  //   const getUser = () => {
  //     fetch("http://localhost:5000/auth/login/success", {
  //       method: "GET",
  //       credentials: "include",
  //       headers: {
  //         Accept: "application/json",
  //         "Content-Type": "application/json",
  //         "Access-Control-Allow-Credentials": true,
  //       },
  //     })
  //       .then((response) => {
  //         if (response.status === 200) return response.json();
  //         throw new Error("authentication failed");
  //       })
  //       .then((resObject) => {
  //         setUser(resObject.user);
  //       })
  //       .catch((err) => {
  //         {
  //           console.log(err);
  //         }
  //       });
  //   };
  //   getUser();
  // }, []);

  // composeWithDevTools();
  const { currentUser } = useContext(AuthContext);

  const RequireAuth = ({ children }) => {
    return currentUser ? children : <Navigate to="/login" />;
  };

  console.log(currentUser);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <RequireAuth>
                <Dashboard />
              </RequireAuth>
            }
          />
          <Route
            path="/listcardashboard"
            element={
              <RequireAuth>
                <ListCarDashboard />
              </RequireAuth>
            }
          />
          <Route
            path="/adduser"
            element={
              <RequireAuth>
                <New inputs={userInputs} />
              </RequireAuth>
            }
          />
          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
      {/* <BrowserRouter>
        <data.Provider value={{ user, setUser }}>
          <Routes>
            <Route
              path="/login"
              element={user ? <Navigate to="/home" /> : <Login />}
            />
            <Route
              path="/home"
              element={
                user ? (
                  user.isAdmin ? (
                    <Navigate to="/Dashboard" />
                  ) : (
                    <SearchCar />
                  )
                ) : (
                  <Navigate to="/login" />
                )
              }
            />
            <Route
              path="/Dashboard"
              element={user ? <Dashboard /> : <Navigate to="/login" />}
            />
            <Route
              path="/Dashboard/listCarDashboard"
              element={user ? <listCarDashboard /> : <Navigate to="/login" />}
            />
            <Route
              path="/home/result/:id"
              element={user ? <ResultSC /> : <Navigate to="/login" />}
            />
            <Route
              path="/home/result/detail/:id"
              element={user ? <DetailCarPage /> : <Navigate to="/login" />}
            />
            <Route path="/" element={<Navigate to="/Login" />} />
            <Route
              path="/home/result/:id"
              element={
                user ? <Navigate to="/ResultSC" /> : <Navigate to="/login" />
              }
            />
            <Route
              path="/home/result/detail/:id"
              element={<Navigate to="/DetailCarPage" />}
            />
          </Routes>
        </data.Provider>
      </BrowserRouter> */}
    </div>
  );
}

export default App;
