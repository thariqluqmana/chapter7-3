import React from 'react'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

const bull = (
    <Box
      component="span"
      sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
      •
    </Box>
  );
  
  const card = (
    <React.Fragment>
      <CardContent>
        <Typography variant="h5" gutterBottom>
          Tentang Paket
        </Typography>
        <Typography variant="p" component="div"  color="text.secondary">
          include
        </Typography>
        <Typography sx={{ mb: 1.5, ml:'35px', mt:'5px' }} >
          <ul>
            <li>Durasi max 1 abad</li>
            <li>Dilarang merusak</li>
            <li>Dilarang Kabur</li>
            <li>Sudah termasuk pajak</li>
          </ul>
        </Typography>
        <Typography variant="p" component="div"  color="text.secondary">
          Exclude
        </Typography>
        <Typography sx={{ mb: 1.5, ml:'35px', mt:'5px' }} >
          <ul>
            <li>Wajib memberi makanan untuk sopir</li>
            <li>Wajib memberi uang tip minim sejuta</li>
            <li>Sopir harus berbahagia</li>
          </ul>
        </Typography>
        <Typography variant="h5" gutterBottom>
          Refund, Reschedule, Overtime
        </Typography>
        <Typography sx={{ mb: 1.5, ml:'35px', mt:'5px' }} >
          <ul>
            <li>Tidak ada reschedule, salah anda sendiri</li>
            <li>Tidak ada refund, orang duit punya gua</li>
            <li>Overtime selama se abad akan di kenai sangsi</li>
            <li>Dah lah</li>
          </ul>
        </Typography>
      </CardContent>
    </React.Fragment>
  );
function CardsDetail() {
  return (
    <div className='CardsDetail'>
        <Box sx={{ minWidth: 275 }}>
      <Card variant="outlined">{card}</Card>
    </Box>
    </div>
  )
}


export default CardsDetail