import React from "react";
import Sidebar from "./Sidebar/Sidebar";
import "./dashboard.scss";
import Widget from "./Widget/Widget";
import Navbar from "./Navbar/Navbar";
import Featured from "./Featured/Featured";
import Chart from "./Chart/Chart";
import Tables from "./table/Tables";
import Datatable from "./Datatable/Datatable";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";

const Dashboard = () => {
  return (
    <div className="Dashboard">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        <div className="widgets">
          <Widget type="user" />
          <Widget type="order" />
          <Widget type="earning" />
          <Widget type="balance" />
        </div>
        <div className="charts">
          <Featured />
          <Chart aspect={2 / 1} />
        </div>
        <Link to="/adduser" style={{ textDecoration: "none" }}>
          <div className="button" style={{ paddingLeft: 20, paddingTop: 20 }}>
            <Button size="small" variant="contained" color="success">
              <AddCircleIcon />
            </Button>
          </div>
        </Link>
        <Datatable />
      </div>
    </div>
  );
};

export default Dashboard;
