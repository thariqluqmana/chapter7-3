import React from 'react'
import Logins from './Logins'
import Wallpaper from './Wallpaper'
import './Login.css'

function Login() {
  return (
    <div className='Login'>
      <Logins/>
      <Wallpaper/>
    </div>
  )
}

export default Login