import React from 'react'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

function  ContentSC() {
  return (
    <div className='ContentSC'>
        <Box sx={{
        width: '1300px', marginLeft:'100px '
      }}>
      <Grid container spacing={1}>
        <Grid item xs={6} sx={{ flexGrow: 1, display: 'flex', justifyContent:'center', alignSelf:'center',flexDirection:'column', px:'100px'  }}>
        <Typography variant="h4" component="div" gutterBottom>
        Sewa dan rental mobil terbaikmu di kawasan surabaya
      </Typography>
      <Typography variant="p" component="div" gutterBottom>
      Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
      </Typography>
          
        </Grid>
        <Grid item xs={6}>
        <img src='/images/car.png' width={"700px"}></img>
        </Grid>
        
      </Grid>
    </Box>
    </div>
  )
}

export default  ContentSC