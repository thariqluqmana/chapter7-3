// Import the functions you need from the SDKs you need
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration

import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCEQMyqfCyQN6P-ChR185bjuVzsgbRzn2o",
  authDomain: "final-chapter.firebaseapp.com",
  projectId: "final-chapter",
  storageBucket: "final-chapter.appspot.com",
  messagingSenderId: "4416733258",
  appId: "1:4416733258:web:0e4e34830c6e9c1161a6b3",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const storage = getStorage(app);
export const auth = getAuth();
